\Chapter{INTRODUCTION\label{chap:Introduction}}

Les compagnies a�riennes cherchent des moyens d'optimiser leurs
profits. Nous consid�rons ici les compagnies de transport a�rien de
passagers telles que, par exemple, Air Canada, British Airways et
Delta Airlines. Les revenus de ces transporteurs proviennent
essentiellement des billets vendus � des passagers. Les d�penses,
quant � elles, sont li�es aux employ�s, � l'utilisation d'avions et �
d'autres frais op�rationnels connexes comme les frais d'acc�s aux
a�roports ou encore les primes d'assurances. Les revenus d�pendent
ultimement des horaires de vols qui sont offerts aux passagers. Le
probl�me de maximisation des profits pour ces entreprises commerciales
est donc un probl�me de gestion des op�rations. De par sa nature, un
tel probl�me est complexe. Pour �tre trait�, il est souvent divis� en
sous-probl�mes qui sont r�solus s�quentiellement (\cite{Klabjan}). Ces
�tapes sont les suivantes.

\begin{enumerate}[(i)]
  \item confection de l'horaire des vols\label{prob:horaires_de_vols};
  \item affectation des types d'avion\label{prob:affectation_avions};
  \item construction des rotations
    d'avion\label{prob:rotation_avions};
  \item construction des rotations
    d'�quipage\label{prob:rotation_equipages};
  \item confection des horaires mensuels des membres
    d'�quipage\label{prob:horaires_mensuels}.
\end{enumerate}
Ces �tapes sont d�crites dans les paragraphes suivants.

Le probl�me de \emph{confection de l'horaire des vols}
(\ref{prob:horaires_de_vols}) concr�tise l'offre de service que la
compagnie a�rienne souhaite offrir � une client�le cible. Cette �tape
est cruciale dans l'approximation des revenus de l'entreprise. Cette
offre de service, qui est cyclique sur une semaine pour presque toutes
les semaines d'une saison, m�nera vraisemblablement � des ventes de
billets et procurera un revenu au transporteur. Des �tudes de march�s
sont pr�alablement faites afin d'en d�terminer le contexte, que ce
soit la comp�tition ou les opportunit�s commerciales. En g�n�ral, ce
travail de planification a lieu deux fois par ann�e, soit une fois
pour l'horaire d'�t� et une autre pour l'horaire d'hiver. De plus, des
transitions sont consid�r�es afin de pouvoir passer d'un horaire �
l'autre de mani�re graduelle. Un horaire de vols repr�sente un
ensemble de vols reliant diverses villes entre elles et o� chaque
d�collage et atterrissage suit un certain horaire. Par exemple, dans
le but de servir une client�le de gens d'affaire, il pourrait �tre
d�cid� d'offrir un service entre deux villes donn�es seulement en
d�but et fin de journ�e, durant les jours ouvrables de la semaine
alors que d'autres client�les pourraient plut�t requ�rir une offre de
service uniquement durant la fin de semaine.

Un vol, ou segment de vol, est la donn�e d'un d�collage d'un avion �
partir d'un a�roport donn� et � un instant donn� qui sera suivi d'un
atterrissage � un a�roport de destination � un instant donn�. Un
billet vendu � un passager repr�sente un itin�raire pour celui-ci. Le
passager empruntera un ou plusieurs segments de vol pour effectuer son
trajet.  Avec une approximation du nombre de places occup�es pour un
segment de vol, les planificateurs pourront donc en estimer le revenu
associ�. Le revenu issu de la vente d'un billet peut ensuite �tre
d�compos� par segment de vol via une heuristique. Les co�ts
d'op�rations sont connus pour chaque type d'avion dont l'�quipage est
d�termin�, tout comme les co�ts d'exploitation de l'avion, du
carburant et des routines de maintenance. Le co�t d'un billet varie
par classe de transport; on entend par classe de transport entre
autres, la classe affaire, la classe �conomique ou la premi�re classe.

\begin{figure}[t]
  \begin{center}
    \includegraphics[width=130mm]{dia/Diag1.eps}
    \caption{D�composition en sous-probl�mes avec r�troactions}
    \label{img:sous-problemes-2}
  \end{center}
\end{figure}

Pour r�soudre correctement le probl�me (\ref{prob:horaires_de_vols}),
il faut que celui-ci soit consid�r� conjointement avec le probl�me
d'affectation des types d'avion (\ref{prob:affectation_avions}). En
effet, il se pourrait qu'il n'existe pas d'affectation de types
d'avion aux vols d'un horaire donn� par cause de manque d'avions d'un
certain type. Il peut aussi s'av�rer �conomiquement judicieux de
modifier une affectation des types d'avion � un vol pour en adapter la
capacit� de passagers selon le flot estim�, d'o� l'utilit� d'un mod�le
de flot de passagers, soit un mod�le qui permet d'estimer le nombre de
passagers attendus sur chaque itin�raire de passagers. La figure
\ref{img:sous-problemes-2} pr�sente les interactions ainsi envisag�es
entre ces probl�mes ainsi que la s�quence de r�solution actuellement
d�crite.

Un mod�le de flot de passagers requiert une liste d'itin�raires
potentiellement int�ressants pour des passagers de m�me qu'une
affectation de types d'avion aux vols. Cette affectation de types
d'avion permet d'�tablir une capacit� en termes du nombre de passagers
qui peuvent transiger pour chacun des segments de vol. On fait appel �
un g�n�rateur d'itin�raires pour �num�rer un ensemble d'itin�raires
potentiellement int�ressants pour les voyageurs. Un tel mod�le de flot
peut �tre r�solu par une m�thode de point fixe, comme c'est le cas
dans \cite{Dumas} et il en r�sultera un nombre de places utilis�es
pour chaque segment de vol. Si le flot de passagers pour un segment de
vol donn� est satur� et aurait pu �tre plus �lev�, l'affectation du
type d'avion pourrait �tre revue afin d'accommoder une client�le plus
nombreuse et, id�alement, augmenter les revenus.

Le probl�me d'\emph{affectation des types d'avion}
(\ref{prob:affectation_avions}) consiste � assigner un type d'avion �
chaque vol d�cid� dans l'�tape pr�c�dente. La principale contrainte
dans ce probl�me est la quantit� finie d'avions par type qui est � la
disposition du transporteur. D�s lors que des avions sont affect�s �
des vols, il faut s'assurer que les avions de chaque type puissent
couvrir tous les segments de vol qui leur sont affect�s sans faire des
d�placements � vide, tout en tenant compte des temps d'inutilisation
d�s aux entretiens obligatoires. On parle alors du probl�me de
\emph{construction des rotations d'avion avec contraintes
  d'entretien} (\ref{prob:rotation_avions}). Les entretiens varient
d'une compagnie a�rienne � l'autre selon la l�gislation qui
s'applique. De plus, ces contraintes varient d'un type d'avion � un
autre. Les normes en vigueur prennent habituellement en compte les
donn�es historiques par avion et par compagnie a�rienne afin d'assurer
un suivi plus strict aupr�s des avions et transporteurs ayant pr�sent�
le plus d'anomalies dans le pass�.

Pour qu'ils puissent fonctionner et assurer un service de qualit�, les
avions doivent �tre op�r�s par un �quipage compos� de pilotes,
navigateurs et agents de bord. Il faut s'assurer que ces employ�s
puissent revenir � la maison p�riodiquement; on doit donc r�soudre le
probl�me de \emph{rotations d'�quipage}
(\ref{prob:rotation_equipages}). De plus, des contraintes
additionnelles s'appliquent, que ce soit au niveau des conditions de
travail r�glement�es, de conventions collectives ou des niveaux de
comp�tence des divers postes. En effet, ce ne sont pas tous les
pilotes qui sont habilet�s � piloter n'importe quels types
d'avion. Des consid�rations analogues existent au niveau des agents de
bord. Lorsque chacun des probl�mes mentionn�s ci-haut sont r�solus, il
faut faire la \emph{confection d'horaires mensuels}
(\ref{prob:horaires_mensuels}) pour les �quipages. � cette �tape,
chaque membre d'�quipage se voit attribuer un horaire compos� de
rotations, de jours de cong�s, et possiblement de p�riodes
d'entra�nement et de vacances. Ces horaires peuvent �tre construits de
fa�on anonyme ou personnalis�e en prenant en compte des activit�s
pr�-affect�es aux employ�es et leurs pr�f�rences.

Dans ce m�moire, nous nous int�ressons au probl�me d'�num�ration des
itin�raires dans le but d'alimenter un mod�le de flots de
passagers. Il faut cependant veiller � ne pas �num�rer trop
d'itin�raires car ceci aurait pour effet de ralentir consid�rablement
l'ex�cution d'un mod�le de flots de passagers. Nous cherchons
cons�quemment � trouver une m�thode qui soit rapide afin d'�tre
utilisable dans un contexte de confection des horaires de vols parce
qu'elle sera sollicit�e lors de multiples it�rations afin de s'assurer
d'obtenir une solution r�alisable et efficace. On peut donc penser �
des m�thodes de programmation dynamique avec des strat�gies
d'acc�l�ration.

L'objet du d�nombrement concerne les \emph{itin�raires de
  passagers}. Chacun de ces �l�ments consiste en une suite
d'�v�nements qui surviennent dans le temps et qui permettent � un
voyageur de partir d'un a�roport d'\emph{origine} et de se rendre � un
a�roport de \emph{destination}. Cet itin�raire peut �tre
\emph{direct}, c'est-�-dire qu'il peut se rendre de l'origine vers la
destination sans effectuer d'atterrissage interm�diaire.  Si ce n'est
pas le cas, on dira alors qu'il est \emph{compos�}. Dans ce cas, on
peut avoir un itin�raire avec une ou plusieurs \emph{correspondances}
ou avec une ou plusieurs \emph{escales}. Une \emph{escale} est le cas
de figure o� un passager est dans un avion qui atterrit dans un
a�roport interm�diaire pendant un temps restreint et red�colle plus
tard sans qu'il n'ait eu � changer d'avion. S'il y a changement
d'avion, et donc de vol, on parle plut�t d'une
\emph{correspondance}. La figure \ref{img:ex_itineraire} pr�sente un
exemple d'itin�raire avec correspondance partant de Vancouver, dont le
code d'identification attribu� par l'agence internationale de
transport a�rien (AITA) est YVR, vers Halifax (code YHZ); le premier
segment de vol se fait par le vol AC 123 alors que le second segment
de vol s'effectue par le vol AC 417. La correspondance s'effectue �
Edmonton (YEG).

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=90mm]{images/exemple_itineraire.eps}
    \caption{Exemple d'itin�raire entre Vancouver (YVR) et Halifax
      (YHZ) avec correspondance � Edmonton (YEG)}
    \label{img:ex_itineraire}
  \end{center}
\end{figure}

Lorsque vient le moment de comparer des itin�raires entre eux, il faut
pouvoir les ordonner et nous utilisons pour ce faire la notion de
\emph{d�sagr�ment} d'un itin�raire. Ici, la notion de
\emph{d�sagr�ment} est une mesure plus ou moins abstraite du degr�
d'inconfort, moral ou physique, qu'un passager associe � un
itin�raire. On dira qu'un �v�nement ou un itin�raire est
\emph{d�sagr�able} s'il augmente le d�sagr�ment d'un
passager. Remarquons que cette notion est relative. Par exemple, si un
itin�raire d'une dur�e de deux heures existe entre un a�roport $A$
vers un a�roport $B$, il peut �tre jug� d�sagr�able par rapport � un
second itin�raire d'une dur�e moindre. Inversement, si ce premier
itin�raire s'av�re �tre le plus rapide, alors il est de d�sagr�ment
moindre que d'autres n�cessitant un temps plus long. De la m�me
mani�re, un itin�raire avec escale est moins d�sagr�able qu'un autre
itin�raire avec correspondance o� le passager doit changer d'avion;
survient alors le risque de manquer l'embarquement dans le second
avion si le premier atterrissage s'effectue avec du retard, tout comme
le risque de perte de bagages. Donc, la dur�e totale du vol par
rapport au vol le plus court est un facteur de d�sagr�ment, tout comme
le nombre d'escales et de correspondances, une correspondance �tant
plus d�sagr�able qu'une escale.

Les pr�f�rences des usagers du transport a�rien sont nombreuses et
diff�rent d'une client�le � l'autre. Le but de ce m�moire est
d'�num�rer tous les itin�raires de passagers int�ressants. Nous ne
tenterons pas ici d'�tablir une hi�rarchisation universelle des
pr�f�rences des usagers mais plut�t d'utiliser les principaux crit�res
de d�sagr�ment pour d�terminer si un itin�raire est acceptable ou non.

Le reste de ce m�moire pr�sente une revue de litt�rature dans le
chapitre \ref{sec:revue-litterature}, une description de la
probl�matique et de la mod�lisation dans le chapitre
\ref{chap:Probleme}, suivi des algorithmes d�velopp�s dans le chapitre
\ref{chap:Algos} et des r�sultats num�riques obtenus avec ceux-ci dans
le chapitre \ref{chap:Resultats}. Finalement, une conclusion fait le
point sur les travaux effectu�s et discute de directions de recherches
futures par rapport � ce probl�me.


\clearpage
